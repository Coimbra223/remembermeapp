//
//  Helpers.swift
//  rememberME
//
//  Created by Daniel Coimbra on 10/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import UserNotifications
import AudioToolbox

let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 100))
let imagePadding = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))


public func sendNotification(withTitle title: String?, subtitle: String?,body: String?, teste: String?){
    let content = UNMutableNotificationContent()
    content.title = title!
    content.subtitle = subtitle!
    content.body = body!
    content.badge = 1
	
	AudioServicesPlaySystemSound(1000)
	
    
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let request = UNNotificationRequest(identifier: "AlertObject" + teste!, content: content, trigger: trigger)
    
    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
}

extension UIViewController {
    
    @objc var topDistance: CGFloat {
        return navigationController?.toolbar.frame.height ?? 0 + UIApplication.shared.statusBarFrame.height
    }
    
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func generateTitleView(withTitle title: String, withBottomLabel bottomText: String? , withColor color: UIColor?) {
        
        let centerAligment = NSMutableParagraphStyle()
        centerAligment.alignment = .center
        
        let mColor = color ?? .blue
        
        let myAttribute = [ NSAttributedStringKey.font: UIFont.eurosoft, NSAttributedStringKey.foregroundColor: mColor, NSAttributedStringKey.paragraphStyle:  centerAligment]
        let myString = NSMutableAttributedString(string: title, attributes: myAttribute )
        
        if let text = bottomText {
            
            let attrString = NSAttributedString(string: "\n"+text)
            myString.append(attrString)
            let length = text.characters.count + 1
            let myRange = NSRange(location: title.characters.count, length: length)
            myString.addAttributes([NSAttributedStringKey.font : UIFont.eurosoft.withSize(18), NSAttributedStringKey.foregroundColor: mColor, NSAttributedStringKey.paragraphStyle:  centerAligment], range: myRange)
            
        }
        
        let titleView = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 64, height: 64)))
        titleView.numberOfLines = 0
        titleView.attributedText = myString
        
        navigationItem.titleView = titleView
    }
    @objc func back(){
        navigationController?.popViewController(animated: true)
    }
}

extension UILabel{
    
    var optimalHeight : CGFloat
    {
        get
        {
            let label = UILabel(frame: CGRect(x:0, y: 0,width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            
            label.sizeToFit()
            
            return label.frame.height
        }
    }
}

extension UITextField {
    
    func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
	
    
}

extension UITextView{
    
    var optimalHeight : CGFloat
    {
        get
        {
            let label = UITextView(frame: CGRect(x:0, y: 0,width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            //label.numberOfLines = 0
            //label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            
            label.sizeToFit()
            
            return label.frame.height
        }
    }

}

extension UIFont {
    static var eurosoft: UIFont {
        return UIFont(name: "Eurosoft", size: 18)!
    }
    static  var eurosoftBold: UIFont {
        return UIFont(name: "Eurosoft-SemiboldItalic", size: 15)!
    }
    static  var karla: UIFont {
        return UIFont(name: "Karla", size: 15)!
    }
}

extension UIColor{
	
	convenience init(r: CGFloat, g: CGFloat ,b: CGFloat){
		self.init(red: r/255,green: g/255,blue: b/255 , alpha: 1)
	}
}
extension UIView {
	var dimHeight: CGFloat {
		return frame.height + frame.origin.y
	}
}
extension AddReminderVC: UITextFieldDelegate{
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if textField == radiusInput{
			let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
			let compSepByCharInSet = string.components(separatedBy: aSet)
			let numberFiltered = compSepByCharInSet.joined(separator: "")
			return string == numberFiltered
		}
		return true
	}
}

extension FirstViewVC{
	func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
		DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
			completion()
		}
	}
}
