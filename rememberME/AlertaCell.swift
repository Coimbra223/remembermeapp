//
//  AlertaCell.swift
//  rememberME
//
//  Created by Daniel Coimbra on 13/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import UIKit
import MapKit
import MGSwipeTableCell

class AlertaCell: MGSwipeTableCell {

    @IBOutlet weak var mapaCell: MKMapView!
    @IBOutlet weak var textCell: UILabel!
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var textAddress: UILabel!
    @IBOutlet weak var cellView: UIView!
    
	
    var location: CLLocation?
    
    var obj: AlertObject?
    
    static let identifier = "AlertaCell"
    override func awakeFromNib() {
        super.awakeFromNib()
 
        mapaCell?.mapType = MKMapType.standard
        mapaCell?.isZoomEnabled = false
        mapaCell?.isScrollEnabled = false
    }
    
    func setTitleTextColor(color: UIColor){
        titleCell.textColor = color
    }
    
    func setBackgroundColor(color: UIColor){
        cellView.backgroundColor = color
    }
    
    func getTextCell() -> String{
        return self.textCell.text!
    }
    func getTitleCell()  -> String{
        return self.titleCell.text!
    }
    func getTextAddress()  -> String{
        return self.textAddress.text!
    }
    func getLocation() -> CLLocation{
        return self.location!
    }
    func setTextCell(_ text: String){
        self.textCell.text = text
    }
    func setTitleCell(_ text: String){
        self.titleCell.text = text
    }
    func setTextAdress(_ text: String){
        self.textAddress.text = text
    }
    func setLocation(_ location: CLLocation){
        self.location = location
        mapInit(location)
    }
    func setIndex(_ index: Int){
        self.obj?.index = index
    }
    
    func mapInit(_ location: CLLocation){
        
        let startLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let viewRegion = MKCoordinateRegionMakeWithDistance(startLocation, 1000, 1000)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = startLocation
        
        self.mapaCell?.addAnnotation(annotation)
        self.mapaCell?.setRegion(viewRegion, animated: false)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openMapForPlace))
        tap.numberOfTapsRequired = 1
        mapaCell?.addGestureRecognizer(tap)
    }
    
    
    @objc func openMapForPlace() {
        
        let regionDistance:CLLocationDistance = 1000
        let coordinates = CLLocationCoordinate2DMake((self.location?.coordinate.latitude)!, (self.location?.coordinate.longitude)!)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = obj?.address
        mapItem.openInMaps(launchOptions: options)
        
    }
    
    
    func toggleStatus() {
        defer { obj?.isMonitorized = !(obj?.isMonitorized)!}
    }
    
}
