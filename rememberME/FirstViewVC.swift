//
//  FirstViewVC.swift
//  rememberME
//
//  Created by Daniel Coimbra on 13/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import MGSwipeTableCell
import AudioToolbox
import Alerts


class FirstViewVC: UITableViewController,CLLocationManagerDelegate,MGSwipeTableCellDelegate {
    
	var arrayObjects:[AlertObject] = []
    var coreLocationManger = CLLocationManager()
    var wantedAdress: String?
    var titleLable = UILabel()
	var popView: View? = nil
	var objectBeingModified: AlertObject?
	
	var addressArray:[String: String] = [:]
	var textArray:[String: String ] = [:]
	
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLable.removeFromSuperview()
        fecthResults()
        tableView.reloadData()
        checkEmpty()
		checkIfNew()
		self.view.addSubview(popView!)
    }
    
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification), name: myNotifications.alterarObjecto.name, object: nil)
		
		tableView.register(UINib(nibName: "AlertaCell", bundle: Bundle.main), forCellReuseIdentifier: AlertaCell.identifier)
		tableView.rowHeight = 150
		tableView.tableFooterView = UIView()
		
		navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add"), style: .plain, target: self, action: #selector(self.addReminder))
		self.view.backgroundColor = UIColor(r:228,g:228,b:228)
        
        popView = View(origem: 0, type: PopUpOptions.sucess, title: "")
	}
    
	func checkIfNew(){
		let delegate = UIApplication.shared.delegate as! AppDelegate
		let context = delegate.persistentContainer.viewContext
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
		var iterator = 0
		for object in arrayObjects{
			if object.justAdded == true{
				self.popView?.handleOptionChange(.sucess, "Reminder added with sucess",seconds: 4)
				do{
					let results = try context.fetch(request) as! [NSManagedObject]
					results[iterator].setValue(false, forKey: "justAdded")
					try context.save()
				} catch{
					print("error")
				}
			}
			iterator = iterator + 1
		}
	}
	
	@objc func handleNotification(_ notification: Notification) {
		let objectoModificado = notification.object as! AlertObject
		setMonitoredRegionStatus(location: objectoModificado.location!, true, objectoModificado.title!)
	}
    func checkEmpty(){
        if arrayObjects.count == 0{
            titleLable = UILabel (frame: CGRect(x: 0 , y: self.topDistance + 16 , width: self.view.frame.width - 20 * 2 ,height: 45))
            titleLable.text = "Press Add button to add some reminderes"
            titleLable.textColor = .lightGray
            titleLable.center.x = view.center.x
            titleLable.textAlignment = .center
            self.view.addSubview(titleLable)
        }
            self.generateTitleView(withTitle: "", withBottomLabel: "Reminders(\(arrayObjects.count))", withColor: UIColor(r:61,g:91,b:151))
    }
   @objc func addReminder(){
		if arrayObjects.count >= 1{
			self.popView?.handleOptionChange(.error, "You have too many Reminders in use, delete some so that you can create some more!\nKeep in mind that the maximum is 20 reminders!",seconds: 4)
		}else {
			let controller = AddReminderVC()
			navigationController?.pushViewController(controller, animated: true)
		}
    }
    
    func fecthResults(){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        request.returnsObjectsAsFaults = false
        arrayObjects = []
		addressArray = [:]
		textArray = [:]
        
        do{
            let results = try context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let titulo = result.value(forKey: "title") as! String
                    let text = result.value(forKey: "text") as! String
                    let address = result.value(forKey: "address") as! String
                    let longitude = result.value(forKey: "longitude") as! Double
                    let latitude = result.value(forKey: "latitude") as! Double
                    let date = result.value(forKey: "date") as! String
                    let isMonitorized = result.value(forKey: "isMonitorized") as! Bool
					let radius = result.value(forKey: "radius") as! Double
					let justAdded = result.value(forKey: "justAdded") as! Bool
                    self.arrayObjects.append(AlertObject.init(titulo, address, CLLocation(latitude:  latitude, longitude: longitude), text,date,isMonitorized,radius,justAdded))
                     setMonitoredRegionStatus(location: CLLocation(latitude: latitude, longitude: longitude), !isMonitorized,titulo)
                     wantedAdress = address
					self.addressArray[titulo] = address
					self.textArray[titulo] = text
                }
            }
        }catch{
            print("erro")
		}
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayObjects.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AlertaCell.identifier) as! AlertaCell! else { return  UITableViewCell() }
        let obj = arrayObjects[indexPath.row]
        cell.obj = obj
        cell.setTextCell(obj.text!)
        cell.setTitleCell(obj.title!)
        cell.setTextAdress(obj.address!)
        cell.setLocation(obj.location!)
        cell.setIndex(indexPath.row)
        
        cell.setBackgroundColor(color: cell.obj?.isMonitorized ?? false ? UIColor(r:61,g:91,b:151) : .gray )
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        
        cell.separatorInset = UIEdgeInsetsMake(0,1000, 0, 0)
        cell.delegate = self
		
		
        cell.leftButtons = [
            MGSwipeButton(title: cell.obj?.isMonitorized ?? false ? "stop" : "start", backgroundColor: cell.obj?.isMonitorized ?? false ? .red : .green, callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                do{
                    let results = try context.fetch(request) as! [NSManagedObject]
                    results[indexPath.row].setValue(cell.obj?.isMonitorized ?? false ? false : true, forKey: "isMonitorized")
                    try context.save()
					
                    self.setMonitoredRegionStatus(location: obj.location!, cell.obj?.isMonitorized ?? false, obj.title!)
                    cell.setBackgroundColor(color: cell.obj?.isMonitorized ?? false ? UIColor(r:61,g:91,b:151) : .gray )
                    cell.toggleStatus()
                    tableView.reloadData()
					if (cell.obj?.isMonitorized)! {
						AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
					}
                }catch{
                    print("erros")
                }
                return true
            })]
		
		
        cell.leftSwipeSettings.transition = .clipCenter
		
        cell.rightButtons = [MGSwipeButton(title: "Delete", backgroundColor: UIColor.red, callback: {
            (sender: MGSwipeTableCell!) -> Bool in
            do{
                let results = try context.fetch(request)
                var iterator = 0
                for result in results as! [NSManagedObject]{
                    if iterator == indexPath.row{
                        self.setMonitoredRegionStatus(location: obj.location!, true, obj.title!)
                        context.delete(result)
                        self.arrayObjects.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        self.checkEmpty()
						self.popView?.handleOptionChange(.sucess, "Reminder removed with sucess",seconds: 4)
                    }
                    iterator = iterator + 1
                }
            }catch{
                print("erro")
            }
            return true
        })]
		
        cell.rightSwipeSettings.transition = .clipCenter
        return cell
	}
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		objectBeingModified = arrayObjects[indexPath.row]
        let controller = IndividualRemiderVC()
        controller.obj = objectBeingModified
        navigationController?.pushViewController(controller, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    
    
    func setMonitoredRegionStatus(location:CLLocation, _ status: Bool,_ identifier: String) {
        let startLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let monitoredRegion = CLCircularRegion(center: startLocation, radius: 100, identifier: identifier )
        if !status {
            coreLocationManger.requestAlwaysAuthorization()
            coreLocationManger.startMonitoring(for: monitoredRegion)
            coreLocationManger.allowsBackgroundLocationUpdates = true
            
            coreLocationManger.delegate = self
            monitoredRegion.notifyOnEntry = true
            monitoredRegion.notifyOnExit = true
            print(coreLocationManger.monitoredRegions)
            
        }else{
            coreLocationManger.stopMonitoring(for: monitoredRegion)
            print(coreLocationManger.monitoredRegions)
            print("stoped monitoring", identifier)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if state == .outside{
            print("outside")
        } else if state == .unknown{
            print("unknown")
        } else if state == .inside {
            print("inside")
        }
    }
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("Starting monitoring \(region.identifier)")
        manager.requestState(for: region)
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
		for (titles,addresses) in addressArray{
			if titles == region.identifier{
				for(titles,texts) in textArray{
					if titles == region.identifier{
						sendNotification(withTitle: region.identifier, subtitle: texts, body: "You have entered \(addresses)",teste: region.identifier)
					}
				}
			}
		}
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Exited Region \(region.identifier)")
    }
}
