//
//  ViewController.swift
//  rememberME
//
//  Created by Daniel Coimbra on 10/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import UserNotifications
import CoreData
import KMPlaceholderTextView
import Alerts

class AddReminderVC: UIViewController,CLLocationManagerDelegate,UISearchBarDelegate,UIScrollViewDelegate,MKMapViewDelegate {
	// teste
	var wantedAdress = ""
	var initialLocation:CLLocation?
	
	var circle:MKCircle!
	var mapView: MKMapView?
	var locationManager:LocationManager!
	
	let annotation = MKPointAnnotation()
	var localSearchRequest:MKLocalSearchRequest!
	var localSearch:MKLocalSearch!
	var localSearchResponse:MKLocalSearchResponse!
	var error:NSError!
	var pointAnnotation:MKPointAnnotation!
	var pinAnnotationView:MKPinAnnotationView!
	
	var longTap = UILongPressGestureRecognizer()
	var popView: View? = nil
	var resultsTable: UITableView!
	
	var matchingItems: [MKMapItem] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		mapView = MKMapView(frame: CGRect(x: 0, y: self.topDistance + 40, width: self.view.frame.width, height: 300))
		mapView?.mapType = MKMapType.standard
		mapView?.isZoomEnabled = true
		mapView?.isScrollEnabled = true
		self.mapView?.delegate = self
		
		longTap = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(sender:)))
		mapView?.addGestureRecognizer(longTap)
		
		locationManager = LocationManager.sharedInstance
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back))
		navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "save"), style: .plain, target: self, action:  #selector(self.saveContext))
		self.view.backgroundColor = UIColor.white
		generateTitleView(withTitle: "", withBottomLabel: "Reminder", withColor: UIColor(r:61,g:91,b:151))
		
		self.view.addSubview((mapView)!)
		self.view.addSubview(titleInput)
		self.view.addSubview(descriptionField)
		self.view.addSubview(searchController)
		self.view.addSubview(radiusInput)
		
		popView = View(origem: self.topDistance + 20, type: PopUpOptions.sucess, title: "")
		self.view.addSubview(popView!)
		
		getLocation()
		
		initTableResults()
		searchController.delegate = self
	}
	
	lazy var radiusInput: UITextField = {
		let radiusInput = UITextField(frame: CGRect(x: 16 , y: (self.descriptionField.frame.maxY) + 16, width:self.view.frame.width - 16 * 2 ,height: 45))
		radiusInput.placeholder = "  Set radius here (only numbers)"
		radiusInput.font = UIFont(name: "Karla", size: 18)!
		radiusInput.delegate = self
		
		let imageView = UIImageView(frame: CGRect(x: 7.5 , y: 7.5 , width:30 ,height: 30))
		imageView.image =  #imageLiteral(resourceName: "info")
		imagePadding.addSubview(imageView)
		radiusInput.rightView = imagePadding
		radiusInput.rightViewMode = .always
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(self.info))
		tap.numberOfTapsRequired = 1
		imagePadding.addGestureRecognizer(tap)
		return radiusInput
	}()
	
	lazy var titleInput: UITextField = {
		let adress = UITextField(frame: CGRect(x: 16 , y: (self.mapView?.frame.maxY)! + 16, width:self.view.frame.width - 16 * 2 ,height: 45))
		adress.leftView = paddingView
		adress.leftViewMode = .always
		adress.layer.masksToBounds = true
		adress.placeholder = "Insert Title here"
		adress.font = UIFont(name: "Karla", size: 18)!
		adress.delegate = self
		return adress
	}()
	
	
	lazy var descriptionField: KMPlaceholderTextView = {
		let field = KMPlaceholderTextView(frame: CGRect(x: 16 , y: self.titleInput.frame.maxY + 16 , width:self.view.frame.width - 16 * 2 ,height: 100))
		field.font = UIFont(name: "Karla", size: 18)!
		field.placeholder = "Insert reminder description here"
		field.layer.masksToBounds = true
		field.textContainer.maximumNumberOfLines = 9
		
		return field
	}()
	
	lazy var searchController: UISearchBar = {
		let searchController = UISearchBar(frame: CGRect(x: 0, y: (self.mapView?.frame.minY)!, width: self.view.frame.width, height: 40))
		searchController.isHidden = false
		searchController.center.y = (self.mapView?.frame.minY)!
		searchController.delegate = self
		searchController.isUserInteractionEnabled = true
		
		return searchController
	}()
	
	func initTableResults() {
		
		resultsTable = UITableView(frame: CGRect(x: searchController.frame.origin.x, y: searchController.frame.maxY, width: self.view.frame.width, height: 0), style: .plain)
		resultsTable.tableFooterView = UIView()
		resultsTable.delegate = self
		resultsTable.dataSource = self
		view.addSubview(resultsTable)
	}
	
	func toggleStatusTable(_ status: Bool) {
		UIView.animate(withDuration: 0.2) {
			self.resultsTable.frame.size.height = self.resultsTable.frame.size.height + (status ? self.view.frame.height : -self.view.frame.height)
		}
	}
	
	@objc func info(){
			self.popView?.handleOptionChange(.danger, "Keep in mind that the radius value will define when the application will send you a notifiation! Set a value and add a pin in the map!",seconds:4)
	}
	
	func checkTitleRepetion(title: String) -> Bool{
		let delegate = UIApplication.shared.delegate as! AppDelegate
		let context = delegate.persistentContainer.viewContext
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
		request.returnsObjectsAsFaults = false
		do{
			let results = try context.fetch(request)
			
			if results.count > 0{
				for result in results as! [NSManagedObject]{
					let titulo = result.value(forKey: "title") as! String
					if titulo == title{
						return true
					}
				}
			}
		}catch{
			print("erro")
		}
		return false
	}
	
	@objc func saveContext () {
		if titleInput.text! != "" && self.wantedAdress != "" && checkTitleRepetion(title: titleInput.text!) == false {
			let delegate = UIApplication.shared.delegate as! AppDelegate
			
			let context = delegate.persistentContainer.viewContext
			
			let date = Date()
			let formatter = DateFormatter()
			formatter.dateFormat = "dd.MM.yyyy"
			let result = formatter.string(from: date)
			
			let newReminder = NSEntityDescription.insertNewObject(forEntityName: "Reminder", into: context)
			
			newReminder.setValue(descriptionField.text, forKey: "text")
			newReminder.setValue(titleInput.text, forKey: "title")
			newReminder.setValue(self.wantedAdress, forKey: "address")
			newReminder.setValue(annotation.coordinate.latitude, forKey: "latitude")
			newReminder.setValue(annotation.coordinate.longitude, forKey: "longitude")
			newReminder.setValue(result, forKey: "date")
			newReminder.setValue(true, forKey: "isMonitorized")
			newReminder.setValue(Double(radiusInput.text!) ?? 100, forKey: "radius")
			newReminder.setValue(true, forKey: "justAdded")
			do {
				try context.save()
				print("saved")
			} catch {
				let nserror = error as NSError
				fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
			}
			navigationController?.popViewController(animated: true)
		} else{
			self.popView?.handleOptionChange(.error, "You cant leave the title field empty or the map without any location! To add a location press for a few seconds the map! And de title cant be repeated with other reminders titles",seconds: 4)
		}
	}
	
	@objc func handleLongPress(sender:UILongPressGestureRecognizer) {
		if (sender.state == UIGestureRecognizerState.ended) {
			addPointer(gestureRecognizer:sender)
		}
	}
	
	func afterSearch(){
		self.resultsTable.frame.size.height = 0
		self.resultsTable.reloadData()
		self.cancelEditing()
	}
	
	func setLocationWithAnnotation(location: CLLocation, withTitle text: String){
		if circle != nil{
			mapView?.remove(circle)
		}
		self.annotation.coordinate = location.coordinate
		self.annotation.title = text
		
		self.mapView?.addAnnotation(annotation)
		let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
		let viewRegion = MKCoordinateRegionMakeWithDistance(pointLocation, 300, 300)
		self.mapView?.setRegion(viewRegion, animated: true)
		setRadius(newCoordinates: pointLocation)
		
		getAddress(location, { (address) in
			self.wantedAdress = address
			self.searchController.text = address
		})
	}
	
	func getLocation(){
		locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
			self.initialLocation =  CLLocation(latitude: latitude, longitude: longitude)
			self.mapInit(location: self.initialLocation!)
		}
	}
	
	func distanceBetweenTwoLocations(current:CLLocation,destination:CLLocation) -> Double{
		let distanceMeters = current.distance(from: destination)
		let distanceKM = distanceMeters / 1000
		return distanceKM
	}
	
	func mapInit(location: CLLocation){
		let startLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
		let viewRegion = MKCoordinateRegionMakeWithDistance(startLocation, 300, 300)
		mapView?.setRegion(viewRegion, animated: false)
	}
	
	func addPointer(gestureRecognizer:UIGestureRecognizer){
		if circle != nil{
			self.mapView?.remove(circle)
		}
		let touchPoint = gestureRecognizer.location(in: mapView)
		let newCoordinates = mapView?.convert(touchPoint, toCoordinateFrom: mapView)
		let location = CLLocation(latitude: (newCoordinates?.latitude)!, longitude: (newCoordinates?.longitude)!)
		setLocationWithAnnotation(location: location, withTitle: "")
	}
	
	func setRadius(newCoordinates: CLLocationCoordinate2D){
		let firstNumberConv :Double? = Double(radiusInput.text!)
		circle = MKCircle(center: newCoordinates, radius: firstNumberConv ?? 100)
		self.mapView?.add(circle)
	}
	
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		let circleRenderer = MKCircleRenderer(overlay: overlay)
		circleRenderer.fillColor = UIColor(r:61,g:91,b:151).withAlphaComponent(0.1)
		circleRenderer.strokeColor = UIColor(r:61,g:91,b:151)
		circleRenderer.lineWidth = 1
		return circleRenderer
	}
	
	func getAddress(_ location: CLLocation, _ completion: @escaping (String) -> ()){
		locationManager.reverseGeocodeLocationWithCoordinates(location, onReverseGeocodingCompletionHandler: { (reverseGecodeInfo, placemark, error) -> Void in
			//print(reverseGecodeInfo?.object(forKey: "streetNumber") as! String) Nome da Rua subLocality
			if reverseGecodeInfo != nil{
				let address = reverseGecodeInfo?.object(forKey: "formattedAddress") as! String //streetNumber
				completion(address)
			}
		})
	}
	
	func cancelEditing(){
		searchController.showsCancelButton = false
		self.view.endEditing(true)
	}
}
