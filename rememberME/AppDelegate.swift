//
//  AppDelegate.swift
//  rememberME
//
//  Created by Daniel Coimbra on 10/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import UIKit
import UserNotifications
import CoreData
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		UIApplication.shared.applicationIconBadgeNumber = 0;
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
		IQKeyboardManager.shared().isEnabled = true
        window?.rootViewController = UINavigationController(rootViewController: FirstViewVC())
        
        registerForRemoteNotification()
		
        return true
    }
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
					DispatchQueue.main.async(execute: {
						 UIApplication.shared.registerForRemoteNotifications()
					})
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
	
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate :UNUserNotificationCenterDelegate {
	
	
	//for displaying notification when app is in foreground
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
	{
		
		completionHandler(
			[UNNotificationPresentationOptions.alert,
			 UNNotificationPresentationOptions.sound,
			 UNNotificationPresentationOptions.badge])
		
	}
	
	
	
	// For handling tap and user actions
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		
		print(response.notification.request.identifier)
		
		switch response.actionIdentifier {
		case "action1":
			print("Action First Tapped")
		case "action2":
			print("Action Second Tapped")
		default:
			break
		}
//
//		if response.notification.request.identifier.contains("AlertObject") {
//			//window?.rootViewController = UINavigationController(rootViewController: vc)
//			//window?.rootViewController?.present(vc, animated: true, completion: nil)
//		}
//
		
		completionHandler()
	}
	
}





