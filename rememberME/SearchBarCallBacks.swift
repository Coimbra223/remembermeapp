//
//  SearchBarCallBacks.swift
//  rememberME
//
//  Created by Daniel Coimbra on 31/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import Foundation
import MapKit


extension AddReminderVC{
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		var annotation:MKAnnotation!
		searchBar.resignFirstResponder()
		dismiss(animated: true, completion: nil)
		if self.mapView?.annotations.count != 0{
			annotation = self.mapView?.annotations[0]
			self.mapView?.removeAnnotation(annotation)
		}
		localSearchRequest = MKLocalSearchRequest()
		localSearchRequest.naturalLanguageQuery = searchBar.text
		localSearch = MKLocalSearch(request: localSearchRequest)
		localSearch.start { (localSearchResponse, error) -> Void in
			if localSearchResponse == nil{
				let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.alert)
				alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
				self.present(alertController, animated: true, completion: nil)
				return
			}
			self.annotation.title = searchBar.text
			self.annotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:localSearchResponse!.boundingRegion.center.longitude)
			let location = CLLocation(latitude: self.annotation.coordinate.latitude, longitude: self.annotation.coordinate.longitude)
			self.setLocationWithAnnotation(location: location, withTitle: searchBar.text!)
			self.afterSearch()
		}
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		let request = MKLocalSearchRequest()
		request.naturalLanguageQuery = searchController.text
		request.region = (mapView?.region)!
		
		let search = MKLocalSearch(request: request)
		
		search.start(completionHandler: {(response, error) in
				self.matchingItems = response?.mapItems ?? []
				self.resultsTable.reloadData()
		})
	}
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		searchBar.showsCancelButton = true
		toggleStatusTable(true)
	}
	
	func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
		cancelEditing()
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
		cancelEditing()
		toggleStatusTable(false)
	}
}
