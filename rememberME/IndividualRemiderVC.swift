//
//  IndividualRemiderVC.swift
//  rememberME
//
//  Created by Daniel Coimbra on 13/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import UIKit
import MapKit
import CoreData


enum myNotifications {
	case alterarObjecto
	var name: Notification.Name {
		switch self {
		case .alterarObjecto:
			return  Notification.Name(rawValue: "ObjectoAlterado")
		}
	}
}
class IndividualRemiderVC: UIViewController, UITextFieldDelegate,MKMapViewDelegate,UITextViewDelegate {
    
    var obj: AlertObject?
    var mapView: MKMapView?
    var circle:MKCircle!
	var oldIdentifier: String?
	
    lazy var dateLabel: UILabel = {
        
        let textLable = UILabel (frame: CGRect(x: self.view.frame.width - 100 , y:  (self.mapView?.frame.maxY)! + 16 , width: 100 ,height:  0))
        textLable.text = self.obj?.date!
        textLable.font = UIFont(name: "Eurosoft-SemiboldItalic" , size: 16)
        textLable.frame.size.height = textLable.optimalHeight
        
        return textLable
    }()
    
    
    lazy var titleLable: UITextView = {
         let titleLable = UITextView (frame: CGRect(x: 16 , y: self.dateLabel.frame.maxY , width: self.view.frame.width - 16 * 2 ,height: 100))
        titleLable.setContentOffset(CGPoint.zero, animated: false)
		titleLable.textContainer.maximumNumberOfLines = 3
        titleLable.text = self.obj?.title!
        titleLable.font = UIFont(name: "Eurosoft-SemiboldItalic" , size: 20)
        titleLable.textColor = UIColor(r:61,g:91,b:151)
        titleLable.frame.size.height = titleLable.optimalHeight
        titleLable.isUserInteractionEnabled = false
        titleLable.delegate = self
        
        return titleLable
        
    }()
    
    
    lazy var addressLable: UILabel = {
        let addressLable = UILabel (frame: CGRect(x: 20 , y: self.titleLable.frame.maxY  , width: self.view.frame.width - 16 * 2 ,height: 0))
        addressLable.text = self.obj?.address!
        addressLable.numberOfLines = 0
        addressLable.font = UIFont(name: "karla" , size: 16)
		addressLable.textColor = .lightGray
        addressLable.frame.size.height = addressLable.optimalHeight
        
        return addressLable
    }()
    
    lazy var textLable: UITextView = {
        let textLable = UITextView (frame: CGRect(x: 16 , y: self.addressLable.frame.maxY + 16, width: self.view.frame.width - 16 * 2 ,height: 0))
		textLable.textContainer.maximumNumberOfLines = 9
        textLable.text = self.obj?.text!
        textLable.font = UIFont(name: "karla" , size: 16)
        textLable.delegate = self
        textLable.textAlignment = NSTextAlignment.justified
        textLable.frame.size.height = textLable.optimalHeight
        textLable.isUserInteractionEnabled = false
		
		
        
        return textLable
    }()
    
    
    @objc func edit(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "save"), style: .plain, target: self, action: #selector(self.save))
        titleLable.isUserInteractionEnabled = true
        textLable.isUserInteractionEnabled = true
        textLable.frame.size.height = 200
		
		oldIdentifier = titleLable.text
    }
	
    
    @objc func save(){
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
		
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(self.edit))
        titleLable.isUserInteractionEnabled = false
        textLable.isUserInteractionEnabled = false
        do{
            let results = try context.fetch(request) as! [NSManagedObject]
            results[(self.obj?.index)!].setValue(textLable.text, forKey: "text")
            results[(self.obj?.index)!].setValue(titleLable.text, forKey: "title")
            try context.save()
        }catch{
            print("error")
        }
        navigationController?.popViewController(animated: true)
		NotificationCenter.default.post(name: myNotifications.alterarObjecto.name, object: self.obj)
		
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.automaticallyAdjustsScrollViewInsets = false
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(self.edit))
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back))
        generateTitleView(withTitle: "", withBottomLabel: "Reminder", withColor: UIColor(r:61,g:91,b:151))
        
        mapView = MKMapView(frame: CGRect(x: 0, y: self.topDistance, width: self.view.frame.width, height: 300))
        mapView?.mapType = MKMapType.standard
        mapView?.isZoomEnabled = true
        mapView?.isScrollEnabled = true
        self.mapView?.delegate = self
        mapInit((self.obj?.location!)!)
        
        self.view.addSubview(titleLable)
        self.view.addSubview(addressLable)
        self.view.addSubview(textLable)
        self.view.addSubview(mapView!)
        self.view.addSubview(dateLabel)
    }
    
    @objc func goBack(){
        navigationController?.popViewController(animated: true)
    }
    
    func mapInit(_ location: CLLocation){
        
        let startLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let viewRegion = MKCoordinateRegionMakeWithDistance(startLocation, (self.obj?.radius)! * 2.4, (self.obj?.radius)! * 2.6)
        
		circle = MKCircle(center: startLocation, radius: (self.obj?.radius)!)
        self.mapView?.add(circle)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = startLocation
        
        mapView?.addAnnotation(annotation)
        mapView?.setRegion(viewRegion, animated: false)
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.fillColor = UIColor(r:61,g:91,b:151).withAlphaComponent(0.1)
        circleRenderer.strokeColor = UIColor(r:61,g:91,b:151)
        circleRenderer.lineWidth = 1
        return circleRenderer
    }
}
