//
//  AlertObject.swift
//  rememberME
//
//  Created by Daniel Coimbra on 13/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import Foundation
import CoreLocation

@objc class AlertObject: NSObject{
    
    var title: String?
    var address: String?
    var location: CLLocation?
    var text: String?
    var date: String?
    var isMonitorized: Bool?
    var index: Int?
	var radius: Double?
	var justAdded: Bool?
    
	init(_ title: String, _ address: String, _ location: CLLocation, _ text: String, _ date: String,_ isMonitorized: Bool,_ radius: Double,_ justAdded: Bool ) {
        self.address = address
        self.title = title
        self.location = location
        self.text = text
        self.date = date
        self.isMonitorized = isMonitorized
		self.radius = radius
		self.justAdded = justAdded
    }
}
