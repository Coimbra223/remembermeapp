//
//  SearchTableSettings.swift
//  rememberME
//
//  Created by Daniel Coimbra on 31/07/17.
//  Copyright © 2017 Daniel Coimbra. All rights reserved.
//

import MapKit
import UIKit


// Implementação dos callbacks e funções necessárias para popular a tabela
extension AddReminderVC: UITableViewDelegate, UITableViewDataSource {
	
	func parseAddress(selectedItem:MKPlacemark) -> String {
		// put a space between "4" and "Melrose Place"
		let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
		// put a comma between street and city/state
		let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
		// put a space between "Washington" and "DC"
		let secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? " " : ""
		let addressLine = String(
			format:"%@%@%@%@%@%@%@",
			// street name
			selectedItem.thoroughfare ?? "",
			comma,
			// street number
			selectedItem.subThoroughfare ?? "",
			firstSpace,
			// city
			selectedItem.locality ?? "",
			secondSpace,
			// state
			 ""
		)
		return addressLine
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
		let item = matchingItems[indexPath.row]

		cell.detailTextLabel?.text = parseAddress(selectedItem: matchingItems[indexPath.row].placemark)
		cell.textLabel?.text = item.name
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		self.cancelEditing()
		self.resultsTable.reloadData()
		
		let locationObjeto = CLLocation(latitude:matchingItems[indexPath.row].placemark.coordinate.latitude , longitude: matchingItems[indexPath.row].placemark.coordinate.longitude)
		setLocationWithAnnotation(location: locationObjeto, withTitle: matchingItems[indexPath.row].name!)
		self.afterSearch()
		
	}
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return matchingItems.count
	}
}
